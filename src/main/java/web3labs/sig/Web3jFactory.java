package web3labs.sig;

import okhttp3.OkHttpClient;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;

public class Web3jFactory {

    public static Web3j Build() {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .authenticator((route, response) -> {
                    String credential = okhttp3.Credentials.basic("epirus", "epirus-rocks");
                    return response.request().newBuilder().header("Authorization", credential).build();
                })
                .build();

        return Web3j.build(new HttpService("https://rinkeby.geth.epirus.web3labs.com", client));
    }

    public static Credentials Credentials(){
        return Credentials.create("74c0ec753e68b1692bfab8f939bedb7a538b9cf008cd6df1f0b9a59f914744a9");
    }
}
