pragma solidity ^0.4.2;

contract CoffeeSupplyChain
{
    event Inspection(address indexed user, string batchNo);
    event Harvesting(address indexed user, string batchNo);
    event Exporting(address indexed user, string batchNo);

    function setInspector(string _batchNo,
                                    string memory _coffeeFamily,
                                    string memory _typeOfSeed,
                                    string memory _fertilizerUsed) public {
        emit Inspection(msg.sender, _batchNo);
    }


    function setHarvester(string _batchNo) public {
        emit Harvesting(msg.sender, _batchNo);
    }

    function setExporter(string _batchNo,
                                uint256 _quantity,
                                uint256 _exporterId) public {
        emit Exporting(msg.sender, _batchNo);
    }

}
