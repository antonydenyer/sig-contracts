package web3labs.sig;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.web3j.coffeesupplychain.CoffeeSupplyChain;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.tx.gas.DefaultGasProvider;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CoffeeSupplyChainContractTest {

    private final static Logger logger = Logger.getLogger(String.valueOf(CoffeeSupplyChainContractTest.class));

    @Test
    void testDeploy() throws Exception {
        Web3j web3j = Web3jFactory.Build();
        Credentials credentials = Web3jFactory.Credentials();

        CoffeeSupplyChain coffeeSupplyChain = CoffeeSupplyChain
                    .deploy(web3j,
                            credentials,
                            new DefaultGasProvider()
                    ).send();

        logger.log(Level.INFO, "CoffeeSupplyChain contract has been deployed to: " +
                coffeeSupplyChain.getContractAddress());

        coffeeSupplyChain.setInspector("123","Arabica","One","20-10-10").send();

        assertTrue(false);
    }
}