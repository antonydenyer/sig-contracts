#!/usr/bin/env bash

cd src/main/resources/solidity/contracts/ && \
    solc --metadata --bin --abi --optimize --overwrite CoffeeSupplyChain.sol -o build/ && \
    web3j solidity generate \
        --binFile build/SupplyChainStorage.bin \
        --abiFile build/SupplyChainStorage.abi \
        -p web3labs.sig.contracts.generated \
        -o ../../../java/